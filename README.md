## About This Profile

This profile instantiates POWDER's Massive MIMO base station (Skylark Wireless Faros(TM) with 64 antennas), and 2 Iris SDR clients connected to a d740 machine.
The machine runs Ubuntu 18.04.
The profile also fetches the latest RENEWLab software with a wide variety of tools to work with Faros Massive MIMO base station, including MATLAB scripts for over-the-air many-antenna experiments, large-scale channel measurement, and many python tools for test and experimentation. 
To learn more about RENEWLab, see [RENEW Documentation](https://docs.renew-wireless.org)

## Getting Started

After logging into pc1, RENEWLab software source is available at /local/repository (it's a root directory).
You can additionally clone the RENEWLab repository from [here](https://gitlab.renew-wireless.org) in your own home directory.
To use any of the design flows, including real-time channel measurement, matlab or python flows, follow the corresponding documentation pages in [RENEW Documentation](https://docs.renew-wireless.org).
